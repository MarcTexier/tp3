# TP 3 - Routage, ARP, Spéléologie réseau

## Préparation de l'environnement

La carte nat est désactivé : 
```bash
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state down group default qlen 1000        
link/ether 08:00:27:18:a1:fc brd ff:ff:ff:ff:ff:ff
```
Le serveur ssh ecoute sur le port 7777 : 

```bash
[centos@router ~]$ sudo ss -l -t -n -p 
State      Recv-Q Send-Q               Local Address:Port                              Peer Address:Port
LISTEN     0      100                      127.0.0.1:25                                           *:*                   
users:(("master",pid=1273,fd=13))
LISTEN     0      128                              *:7777                                         *:*                   
users:(("sshd",pid=1036,fd=3))
LISTEN     0      100                          [::1]:25                                        [::]:*                   
users:(("master",pid=1273,fd=14))
LISTEN     0      128                           [::]:7777                                      [::]:*                   
users:(("sshd",pid=1036,fd=4))
```
Pare-feu activé et configuré : 

```bash
[centos@router ~]$ sudo firewall-cmd --list-all
public (active)
	target: default
	icmp-block-inversion: no
	interfaces: enp0s8
	sources:                                                         
	services: dhcpv6-client ssh
	ports: 7777/tcp
	protocols:
	masquerade: no
	forward-ports:
	source-ports:
	icmp-blocks:
	rich rules:
```

Nom configuré : 
```bash
[centos@localhost ~]$ hostname  
router.tp3  
```

Fichiers /etc/hosts de toutes les machines configurés : 

```bash
[centos@localhost ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
10.3.1.11   client1.net1.tp3
10.3.2.11   server1.net2.tp3
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
```

Liste des pings : 

Client1 vers router : 
```bash
[centos@client1 ~]$ ping router.tp3
PING router.tp3 (10.3.1.254) 56(84) bytes of data.
64 bytes from router.tp3 (10.3.1.254): icmp_seq=1 ttl=64 time=0.735 ms
64 bytes from router.tp3 (10.3.1.254): icmp_seq=2 ttl=64 time=0.886 ms
64 bytes from router.tp3 (10.3.1.254): icmp_seq=3 ttl=64 time=0.833 ms
64 bytes from router.tp3 (10.3.1.254): icmp_seq=4 ttl=64 time=0.299 ms
^C
--- router.tp3 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3012ms
rtt min/avg/max/mdev = 0.299/0.688/0.886/0.231 ms
```

Router vers client1 : 
```bash
[centos@router ~]$ ping client1.net1.tp3
PING client1.net1.tp3 (10.3.1.11) 56(84) bytes of data.
64 bytes from client1.net1.tp3 (10.3.1.11): icmp_seq=1 ttl=64 time=0.384 ms
64 bytes from client1.net1.tp3 (10.3.1.11): icmp_seq=2 ttl=64 time=0.857 ms
64 bytes from client1.net1.tp3 (10.3.1.11): icmp_seq=3 ttl=64 time=0.826 ms
^C
--- client1.net1.tp3 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 0.384/0.689/0.857/0.216 ms 
```

---

&nbsp;

#### Maintenant on peut vérifier qu'au sein de notre lab le client1 peut ping router :

```bash
[centos@router ~]$ ping 10.3.1.254 -c 3
PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=1.33 ms
64 bytes from 10.3.1.254: icmp_seq=2 ttl=64 time=2.67 ms
64 bytes from 10.3.1.254: icmp_seq=3 ttl=64 time=1.25 ms

--- 10.3.1.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 1.253/1.753/2.678/0.656 ms
```

Et inversement router peut ping client1 :

```bash
[centos@router ~]$ ping 10.3.1.11 -c 3
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=1.23 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=1.36 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=1.33 ms

--- 10.3.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2012ms
rtt min/avg/max/mdev = 1.232/1.309/1.365/0.056 ms
```

On essaye maintenant de ping le router depuis le server1 : 

```bash
[centos@router ~]$ ping 10.3.2.254 -c 3
PING 10.3.2.254 (10.3.2.254) 56(84) bytes of data.
64 bytes from 10.3.2.254: icmp_seq=1 ttl=64 time=1.31 ms
64 bytes from 10.3.2.254: icmp_seq=2 ttl=64 time=2.46 ms
64 bytes from 10.3.2.254: icmp_seq=3 ttl=64 time=1.32 ms

--- 10.3.2.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2009ms
rtt min/avg/max/mdev = 1.314/1.701/2.466/0.541 ms
```

Et inversement : 

```bash
[centos@router ~]$ ping 10.3.2.11 -c 3
PING 10.3.2.11 (10.3.2.11) 56(84) bytes of data.
64 bytes from 10.3.2.11: icmp_seq=1 ttl=64 time=1.44 ms
64 bytes from 10.3.2.11: icmp_seq=2 ttl=64 time=1.66 ms
64 bytes from 10.3.2.11: icmp_seq=3 ttl=64 time=1.50 ms

--- 10.3.2.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2007ms
rtt min/avg/max/mdev = 1.441/1.537/1.667/0.095 ms
```

---

## I. Mise en place du routage
### 1. Configuration du routage sur `router`

Tout d'abord on vérifie si l'IP Forwarding est acrivé. Pour cela on tape la command `cat /proc/sys/net/ipv4/ip_forward` : 

```bash
[centos@router ~]$ cat /proc/sys/net/ipv4/ip_forward
0
``` 

Le 0 nous indique que l'IP Forwarding n'est pas activé on tape donc la commande :

```bash
[centos@router ~]$ sudo sysctl -w net.ipv4.ip_forward=1
net.ipv4.ip_forward = 1
```

La machine est donc capable maintenant de "router" des paquets

### 2. Ajouter les routes statiques

On tape la commande `sudo ip route add 10.3.2.0/24 via 10.3.1.254 dev enp0s8` pour joindre le réseau net2, on vérifie avec la commande `ip r s` : 

```bash
[centos@router ~]$ ip r s                                                 
10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.11 metric 101 
10.3.2.0/24 via 10.3.1.254 dev enp0s8
```
On le voit bien apparaître sur la deuxième ligne.

On refait la même étape pour `server1` pour qu'il joigne `net1` en tapant la commande `sudo ip route add 10.3.1.0/24 via 10.3.2.254 dev enp0s8` : 

```bash
[centos@router ~]$ ip r s
10.3.1.0/24 via 10.3.2.254 dev enp0s8 
10.3.2.0/24 dev enp0s8 proto kernel scope link src 10.3.2.11 metric 101
```

Cette fois-ci on peut le vérifier sur la première ligne.

&nbsp;

On vérifie que ça fonctionne bien avec un ping des deux côtés :

```bash
[centos@client1 ~]$ ping 10.3.2.254 -c 3
PING 10.3.2.254 (10.3.2.254) 56(84) bytes of data.
64 bytes from 10.3.2.254: icmp_seq=1 ttl=64 time=1.23 ms
64 bytes from 10.3.2.254: icmp_seq=2 ttl=64 time=1.30 ms
64 bytes from 10.3.2.254: icmp_seq=3 ttl=64 time=1.35 ms

--- 10.3.2.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 1.233/1.296/1.352/0.064 ms
```

Puis avec `traceroute` : 

```bash
[centos@client1 ~]$ traceroute 10.3.2.11
traceroute to 10.3.2.11 (10.3.2.11), 30 hops max, 60 byte packets
 1  gateway (10.3.1.254)  2.140 ms  1.850 ms  1.745 ms
 2  gateway (10.3.1.254)  1.361 ms !X  0.863 ms !X  2.109 ms !X
```

> client1 vers net2

Puis : 
```bash
[centos@client1 ~]$ ping 10.3.1.254 -c 3
PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=2.93 ms
64 bytes from 10.3.1.254: icmp_seq=2 ttl=64 time=1.17 ms
64 bytes from 10.3.1.254: icmp_seq=3 ttl=64 time=1.46 ms

--- 10.3.1.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 1.171/1.854/2.930/0.771 ms
```

Et avec `traceroute` :

```bash
[centos@client1 ~]$ traceroute 10.3.1.11
traceroute to 10.3.1.11 (10.3.1.11), 30 hops max, 60 byte packets
 1  gateway (10.3.2.254)  1.122 ms  0.488 ms  0.823 ms
 2  gateway (10.3.2.254)  0.626 ms !X  0.980 ms !X  1.009 ms !X
```

> server1 vers net1

---

### 3. Comprendre le routage

|             | MAC src       | MAC dst       | IP src       | IP dst       |
| ----------- | ------------- | ------------- | ------------ | ------------ |
| Dans `net1` (trame qui entre dans `router`) | 08:00:27:92:14:91 | 08:00:27:76:e0:78 | 10.3.1.11 | 10.3.2.11 |
| Dans `net2` (trame qui sort de `router`) | 08:00:27:16:98:9d | 08:00:27:33:cd:4e | 10.3.1.11 | 10.3.2.11 |


---

## II. ARP
### 1. Tables ARP
#### Client1 : 

```bash
[centos@client1 ~]$ ip neigh show
10.3.1.254 dev enp0s8 lladdr 08:00:27:76:e0:78 STALE
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:00 REACHABLE
```

`10.3.1.254` : adresse du router
&nbsp;

`dev enp0s8` : en passant par la carte nommée **enp0s8**
&nbsp;

`lladdr` : Acronyme pour l'adresse MAC
&nbsp; 

`08:00:27:76:e0:78` : MAC de la carte **enp0s8** du router
&nbsp;

`0a:00:27:00:00:00` : MAC de la carte host-only 1
&nbsp;

`STALE` : Entrée valable mais suspecte
&nbsp; 

`REACHABLE` : Entrée valable jusqu'à expiration du délai d'accéssibilité

&nbsp;

#### Server1

```bash
[centos@server1 ~]$ ip neigh show 
10.3.2.254 dev enp0s8 lladdr 08:00:27:16:98:9d STALE
10.3.2.1 dev enp0s8 lladdr 0a:00:27:00:00:01 REACHABLE
```


`10.3.2.254` : adresse du router
&nbsp;

`dev enp0s8` : en passant par la carte nommée **enp0s8**
&nbsp;

`lladdr` : Acronyme pour l'adresse MAC
&nbsp;

`08:00:27:16:98:9d` : MAC de la carte **enp0s9** du router
&nbsp;

`0a:00:27:00:00:01` : MAC de la carte host-only 2
&nbsp; 

`STALE` : Entrée valable mais suspecte
&nbsp; 

`REACHABLE` : Entrée valable jusqu'à expiration du délai d'accéssibilité

&nbsp;

#### Router

```bash
[centos@router ~]$ ip neigh show  
10.3.1.11 dev enp0s8 lladdr 08:00:27:92:14:91 STALE
10.3.2.11 dev enp0s9 lladdr 08:00:27:33:cd:4e STALE
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:00 REACHABLE
```

`10.3.1.11` : adresse du client1
&nbsp;

`10.3.2.11` : adresse du server1
&nbsp;

`dev enp0s8` : en passant par la carte nommée **enp0s8**
&nbsp;

`dev enp0s9` : en passant par la carte nommée **enp0s9**
&nbsp;

`lladdr` : Acronyme pour l'adresse MAC
&nbsp; 

`08:00:27:92:14:91` : MAC de la carte enp0s8 de client1
&nbsp;

`08:00:27:33:cd:4e` : MAC de la carte enp0s8 de server1
&nbsp;

`0a:00:27:00:00:00` : MAC de la carte host-only 1
&nbsp; 

`STALE` : Entrée valable mais suspecte
&nbsp;
 
`REACHABLE` : Entrée valable jusqu'à expiration du délai d'accéssibilité

---

### 2. Requêtes ARP
#### A. Table ARP 1

On observe d'abord la table ARP vidé de client1 : 

```bash
[centos@client1 ~]$ sudo ip neigh flush all
[centos@client1 ~]$ ip neigh show          
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:00 REACHABLE
```
Seul le router est joignable, puis on fait un `ping 10.3.2.11 -c 3` pour envoyer 3 paquets a `server1`, on regarde de nouveau la table ARP : 

```bash
[centos@client1 ~]$ ip neigh show
10.3.1.254 dev enp0s8 lladdr 08:00:27:76:e0:78 REACHABLE
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:00 REACHABLE
```

On observe que l'entrée vers `server1` est possible car on lit **REACHABLE**


#### B. Table ARP 2

On vide les tables ARP comme au dessus, puis on tape la commande `ping 10.3.2.11 -c 3` et on obtient une nouvelle ligne dans la table ARP du server1 :

```bash
[centos@server1 ~]$ ip neigh show
10.3.2.254 dev enp0s8 lladdr 08:00:27:16:98:9d REACHABLE
10.3.2.1 dev enp0s8 lladdr 0a:00:27:00:00:01 REACHABLE
```

La première ligne est celle qui dit que l'entrée du router est valable. Et la deuxième ligne vient de s'ajouter suite au `ping` et elle dit que l'entrée du client1 est valable.
